$initial_app = <<INITIAL
  yum -y -q install ruby-devel git gcc-c++
  gem install -V --no-ri --no-rdoc librarian-puppet -v 2.2.3
  mkdir -p /etc/facter/facts.d
  echo 'role=app' >> /etc/facter/facts.d/vagrant.txt
  cd /vagrant/puppet
  /usr/local/bin/librarian-puppet install --verbose
INITIAL

$initial_proxy = <<INITIAL
  yum -y -q install ruby-devel git gcc-c++
  gem install -V --no-ri --no-rdoc librarian-puppet -v 2.2.3
  mkdir -p /etc/facter/facts.d
  echo 'role=proxy' >> /etc/facter/facts.d/vagrant.txt
  cd /vagrant/puppet
  /usr/local/bin/librarian-puppet install --verbose
INITIAL

$run_puppet_app = <<RUNPUPPET
  /opt/puppetlabs/puppet/bin/puppet apply \
  --modulepath=/vagrant/puppet/localmodules:/vagrant/puppet/modules \
  --hiera_config=/vagrant/puppet/hiera.yaml \
  /vagrant/puppet/manifests/site.pp
RUNPUPPET

$run_puppet_proxy = <<RUNPUPPET
  /opt/puppetlabs/puppet/bin/puppet apply \
  --modulepath=/vagrant/puppet/localmodules:/vagrant/puppet/modules \
  --hiera_config=/vagrant/puppet/hiera.yaml \
  /vagrant/puppet/manifests/site.pp
RUNPUPPET


# using snippets of some of my own code in github.com/r3adm3/challenges/blob/master/VM1/Vagrantfile
Vagrant.configure('2') do |config|

   config.vm.define "proxy1" do |proxy|
        
        #sets the source VM to use.
        proxy.vm.box = 'genebean/centos-7-puppet5'
        proxy.vm.box_version = '1.5.0'
        
        #sets the proxy hostname
        proxy.vm.hostname = 'proxy1'

        #setting up networking details
        proxy.vm.network "forwarded_port", guest: 80, host: 18080, host_ip: "127.0.0.1", auto_correct:true
        proxy.vm.network :private_network, ip: "192.168.56.150"

        # setting up proxy memory and network properties in virtualbox
        proxy.vm.provider :virtualbox do |v|
            v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
            v.customize ["modifyvm", :id, "--memory", 512]
            v.customize ["modifyvm", :id, "--name", "proxy1"]
        end

        #stripped out the chef bits, putting in puppet equivalents
        proxy.vm.provision :shell, inline: $initial_proxy
        proxy.vm.provision :shell, inline: $run_puppet_proxy

        #test proxy is running.
        proxy.vm.provision "shell", inline: <<-SHELL
            if lsof -Pi :80 -sTCP:LISTEN -t > /dev/null; then
                echo "proxy1 nginx is running"
            else
                echo "proxy1 nginx is not running"
            fi
        SHELL

    end

  config.vm.define "app1" do |app|

      #sets the source VM to use.
      app.vm.box = 'genebean/centos-7-puppet5'
      app.vm.box_version = '1.5.0'
      
      #sets the proxy hostname
      app.vm.hostname = 'app1'
   
      app.vm.network :private_network, ip: "192.168.56.151"
      app.vm.network 'forwarded_port', guest: 3000, host: 3200

        app.vm.provider :virtualbox do |v|
            v.customize ["modifyvm", :id, "--name", "app1"]
        end

      app.vm.provision :shell, inline: $initial_app
      app.vm.provision :shell, inline: $run_puppet_app

  end




end
