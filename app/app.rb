# app.rb
require 'sinatra'

class App < Sinatra::Base
  get '/' do
    sleep(5)
    'Hello, world! A simple web app and nothing more.'
  end
end
