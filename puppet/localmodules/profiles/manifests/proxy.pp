class profiles::proxy {

  package {'nginx':
    ensure => 'installed',
  }

  package {'lsof':
      ensure=>'installed',
  }

  file {'/etc/nginx/nginx.conf':
    ensure => present,
    source => '/vagrant/proxy/nginx.conf',
  }


  service {'nginx':
    ensure=> running,
  }

}
