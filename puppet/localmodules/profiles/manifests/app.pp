class profiles::app {

  class { '::epel': }->

  yumrepo { 'passenger':
    ensure   => 'present',
    baseurl  => 'https://oss-binaries.phusionpassenger.com/yum/passenger/el/$releasever/$basearch',
    gpgcheck => 0,
  }->

  package { 'pygpgme':
    ensure => 'present',
  }->

  package { 'passenger':
    ensure  => 'installed',
  }->

  package { 'sinatra':
    ensure   => '1.4.7',
    provider => 'gem',
  }->

  # ensures there is an app.rb
  file { '/vagrant/app/app.rb':
    ensure=> 'present',
  }->
  # searches for the original line of content in app.rb and replaces
  # it with the facter role variable. Means this is a bit more reusable than just 
  # hard coding "app" in to the content. 
  file_line {'server role inserted into app.rb':
    path => '/vagrant/app/app.rb',
    replace => true,
    line => "  \'Hello, world! Server is configured to be an $role\'",
    match => '\'Hello, world! A simple web app and nothing more.\'',
  }->

  # We could use a service here but passenger manages the process for us
  # having problems here, can see an error in puppet apply logs about
  # passenger missing using id...but it is on app1 and when started manually
  # does execute !! 
  exec { 'start-passenger':
    command => '/usr/bin/passenger start /vagrant/app',
    unless  => '/bin/ls /tmp/app.pid',
  }

}
